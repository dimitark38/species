import EventEmitter from 'eventemitter3';
import Species from './Species.js';

const Events = {
    MAX_SPECIES_REACH: 'max_species_reached',
    SPECIES_CREATED: 'species_created'
}

export default class StarWarsUniverse extends EventEmitter {

    constructor() {
        super();

        this.species = [];
        this._maxSpecies = 10;

    }

    get speciesCount() {
        return this.species.length;
    }

    async createSpecies() {

        let speciesInst = new Species();
        let url = `https://swapi.booost.bg/api/species/${this.species.length + 1}`;
        speciesInst.on(Species.events.SPECIES_CREATED, () => this._onSpeciesCreated(speciesInst));
        speciesInst.init(url);

    }


    async _onSpeciesCreated(species) {

        //this.on(StarWarsUniverse.events.SPECIES_CREATED, () => {

        if (this.speciesCount === this._maxSpecies) {
            this.emit(StarWarsUniverse.events.MAX_SPECIES_REACH);
            return;
        } else if (this.speciesCount < this._maxSpecies) {

            this.species.push(species);
            await this.createSpecies();
            this.emit(StarWarsUniverse.events.SPECIES_CREATED, { speciesCount: this.speciesCount });
        }

        //});

        //this.emit(StarWarsUniverse.events.SPECIES_CREATED, { speciesCount: this.speciesCount });

    }

    static get events() {
        return Events;
    }
}