import EventEmitter from 'eventemitter3';

const Event = {
    SPECIES_CREATED: 'species_created'
}

export default class Species extends EventEmitter {

    constructor() {
        super();

        this.name = null;
        this.classification = null;

        //this.addListener('species_created', function first() {});

    }


    async init(url) {

        const resp = await fetch(url);
        const data = await resp.json();
        this.name = data.name;
        this.classification = data.classification;

        this.emit(Species.events.SPECIES_CREATED);

        //return data;
    }

    static get events() {
        return Event;
    }
}